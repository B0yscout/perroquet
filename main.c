// Directives pré-processeur
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "header.h"


int main(int argc, char *argv[])
{

    printf("Project C : Perroquet! \n\n");

    int loop = 1;
    while (loop == 1)
    {
        // clear console
        system("clear");

        printf("Que voulez vous faire?:"
            "\n1 : chiffrer un fichier"
            "\n2 : dechiffrer un fichier"
            "\n3 : quitter\n");
        
        int choix;
        scanf("%d",&choix);
        
        // choix utilisateur : 1 pour chiffrer, 2 pour déchiffrer, 3 pour quitter
        
        switch (choix)
        {
            case 1:
                // renseigner le nom du fichier à chiffrer
                printf("\nnom du fichier à chiffrer?\n");
                char fichierSource[LGFIL];
                __fpurge(stdin);
                scanf("%s", fichierSource);

                // renseigner le nom du fichier qui sera généré
                printf("\nnom du fichier destination?\n");
                char fichierDest[LGFIL];
                __fpurge(stdin);
                scanf("%s", fichierDest);

                // renseigner la clé de chiffrage
                printf("\nclé de chiffrage : \n");
                char parrot[LGCLE];
                __fpurge(stdin);
                scanf ("%[^\n]", parrot);

                printf("voter fichier source : %s\n",fichierSource);
                printf("voter fichier destination : %s\n",fichierDest);
                printf("votre clé : %s\n",parrot);

                // enregistrer la clé dans peroq.def
                ecrireCle(parrot, "peroq.def");
                // appel de la fonction de chiffrage
                chiffrer(fichierSource,parrot,fichierDest,0);

                printf("\nAppuyer sur une touche pour continuer");
                char test;
                __fpurge(stdin);
                scanf("%c",&test);

                break;

            case 2:
            
                // renseigner le nom du fichier à dechiffrer
                printf("\nnom du fichier à déchiffrer?\n");
                scanf("%s", fichierSource);

                // Clé dans un fichier ou à rentrer manuellement ? 
                printf("\nClé de chiffrage dans un fichier ? O ou N\n");
                char choixCle;
                __fpurge(stdin);
                scanf ("%c", &choixCle);
                
                if (choixCle == 'O' || choixCle == 'o'){
                    // récupérer clé dans le fichier
                    printf("\nnom du fichier clé?:\n");
                    char fichierCle[LGFIL];
                    scanf("%s", fichierCle);
                    // appel fonction qui renvoie la clé de chiffrement
                    recupCle(fichierCle,parrot);

                }

                else{
                    // renseigner la clé dans le prompt
                    printf("\nclé de chiffrage : \n");
                    
                    __fpurge(stdin);
                    scanf ("%[^\n]", parrot);

                }

                printf("\nvoter fichier source : %s\n",fichierSource);
                printf("votre clé : %s\n",parrot);

                printf("\nsouhaitez vous :\n1 : afficher le résultat\n2 : enregistrer le resultat dans un fichier\n");
                int choixMode = 1;
                scanf("%d",&choixMode);

                if (choixMode == 1)
                {
                    chiffrer(fichierSource,parrot,NULL,1);
                }
                else
                {
                    printf("\nNom du fichier destination?\n");
                    char destination[LGFIL];
                    __fpurge(stdin);
                    scanf ("%[^\n]", destination);
                    chiffrer(fichierSource,parrot,destination,1);
                }

                printf("\nAppuyer sur une touche pour continuer");
                __fpurge(stdin);
                scanf("%c",&test);

                break;   

            case 3:
            default:
                loop = 0;
                break;
        }
        continue;
        
    }

 


    return 0;
}
