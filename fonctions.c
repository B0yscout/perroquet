#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "header.h"

char* recupCle(char* fichier, char* cle)
{
    /*
    * Fonction qui récupère la clé enregistrée dans un fichier
    */
    
    FILE* fp = NULL;
    fp = fopen(fichier, "rt");
    if (fp == NULL)
    {
        printf("\nErreur Open fichier Clé !!");
        exit(1);
    }
   
    // stocker dans variable cle la clé contenue dans le fichier fp 
    fgets(cle, LGCLE, fp);

    int ret = fclose(fp);
    if (ret != 0)
    {
        printf("\nErreur close!");
    }

    return cle;
}

void ecrireCle(char* phrase, char* nomFichier){
    
    /* 
    * Fonction qui enregistre la clé donnée par l'utilisateur
    * dans un fichier dédié. 
    */

    FILE* fp = NULL;
    fp = fopen(nomFichier,"w+t");
    if (fp==NULL){
        printf("\nErreur open");
        exit(1);
    }

    // ecrire la clé dans le fichier fp
    fwrite(phrase, sizeof(char) , strlen(phrase), fp );

    int ret = fclose(fp);
    if (ret != 0)
    {
        printf("\nErreur Close !!");
        exit(1);

    }
    printf("\nvotre clé -%s- est sauvegardée dans le fichier %s.", phrase, nomFichier);
}


void chiffrer(char* source, char* cle, char* dest, int mode)
{
    /* 
    * Fonction qui chiffre (ou déchiffre) en soustrayant (ou additionant)
    * caractère par caractère la source et la clé.
    * en mode chiffrage : enregistre dans fichier dest
    * en mode dechiffrage : enregistre dans dest, ou affiche si dest est NULL.
    */

    FILE* fSource = NULL;
    fSource = fopen(source, "rt");
    if (fSource == NULL)
    {
        printf("\nErreur Open source !!");
        exit(1);
    }

    // si dest == NULL : mode dechiffrage , sans enregistrement
    FILE* fDest = NULL;
    if (dest != NULL)
    {
        fDest = fopen(dest, "w+t");
        if (fDest == NULL)
        {
            printf("\nErreur Open destination !!");
            exit(1);
            
        }
    }

    char lettre;
    fread(&lettre, sizeof(char), 1, fSource);
    if (feof(fSource))
    {
        printf("\nFichier Vide !!!");
    }

    int j = 0;
    char crypt;
    while(!feof(fSource))
    {
              
        // mode 0 : chiffrage
        if (mode == 0)
            crypt = lettre - cle[j];
        else
        // mode 1 : dechiffrage
        if (mode ==1)
            crypt = lettre + cle[j];
        

        if (fDest != NULL){
            // mode ecriture d'un fichier
            fwrite(&crypt, sizeof(char), 1, fDest);
        }
        else{
            // mode affichage uniquement
            printf("%c", crypt);
        }
        
        // itérer sur les caractères de la clé
        crypt+= cle[j];

        // lire lettre suivante
        fread(&lettre, sizeof(char), 1, fSource);

        // initialiser j si on atteint la fin de la chaîne
        if (j != strlen(cle)){
            j++;            
        }
        else{
            j=0;
        }
    }


    // fermer fichiers destination 
    if (fDest != NULL)
    {
        int retD = fclose(fDest);
        if (retD != 0)
        {
            printf("\nErreur Close Destination !!");
            exit(1);

        }
        printf("\nLe fichier %s a été enregistré.\n", dest);
    }


    // fermer fichiers source
    int retS = fclose(fSource);
    if (retS != 0)
    {
        printf("\nErreur Close Source !!");
        exit(1);

    }


    // dans le cas d'un chiffrement : supprimer fichier source
    if (mode==0)
    {
        int del;
        del = remove(source);
        if (del == 0)
        {
            printf("\nle fichier %s a été supprimé.",source);
        }
    }
}



