# perroquet
    Projet C (01-02/12/2022)

# dépôt GIT distant : 
    https://gitlab.com/B0yscout/perroquet

# Fonctionnalités :  

    chiffrement et dechiffrement d'un texte stocké dans un fichier,
    utilisant l'algorithme du perroquet.


    chiffrement : L'utilisateur doit renseigner
        - nom de fichier source
        - nom de fichier destination
        - clé de chiffrage

    dechiffrement :
        - l'utilisateur doit renseigner le nom de fichier à dechiffrer
        - clé : l'utilisateur doit choisir
            - utiliser un fichier existant contenant la clé
            - ou la renseigner manuellement
        - données dechiffrées : l'utilisateur doit choisir 
            - enregistrer dans un fichier destination
            - afficher simplement à l'écran le texte dechiffré.

